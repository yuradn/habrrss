package yurasik.com.observerexample;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import yurasik.com.observerexample.fragment.BackgroundFragment;
import yurasik.com.observerexample.fragment.ListFragment;
import yurasik.com.observerexample.fragment.ParseFragment;


public class MainActivity extends ActionBarActivity {
    public static final String FRAGMENT_ACTIVITY = "activity.fragment";
    public static final String FRAGMENT_BACKGROUND = "background.fragment";
    public static final String FRAGMENT_LIST = "list.fragment";
    public static final String FRAGMENT_DB = "db.fragment";

    public static final String WEB_ADDRESS = "web.address";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        ParseFragment parseFragment = (ParseFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_ACTIVITY);
        if (parseFragment ==null) {
            parseFragment = new ParseFragment();
            FragmentTransaction manager = getSupportFragmentManager().beginTransaction();
            manager.add(R.id.fragmentParse, parseFragment, FRAGMENT_ACTIVITY);
            manager.commit();
        }

        BackgroundFragment backgroundFragment = (BackgroundFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_BACKGROUND);
        if (backgroundFragment ==null) {
            backgroundFragment = new BackgroundFragment();
            FragmentTransaction manager = getSupportFragmentManager().beginTransaction();
            manager.add(backgroundFragment, FRAGMENT_BACKGROUND);
            manager.commit();
        }

        ListFragment listFragment = (ListFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_LIST);
        if(listFragment==null) {
            listFragment = new ListFragment();
            FragmentTransaction manager = getSupportFragmentManager().beginTransaction();
            manager.add(R.id.fragmentDocument,listFragment,FRAGMENT_LIST);
            manager.commit();
        }

    }



}
