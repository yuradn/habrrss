package yurasik.com.observerexample;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class WebViewActivity extends Activity {
    private WebView webView;
    private boolean loading=false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view_layout);


        webView = (WebView) findViewById(R.id.webView);
        webView.setWebViewClient(new MyWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);

        String address = getIntent().getStringExtra(MainActivity.WEB_ADDRESS);
        //startWebView(address);
        webView.loadUrl(address);

//        String customHtml = "<html><body><h1>Hello, WebView</h1></body></html>";
//        webView.loadData(customHtml, "text/html", "UTF-8");

    }




    private class MyWebViewClient extends WebViewClient {
        private ProgressDialog progressDialog;

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            loading=true;

            view.loadUrl(url);
            return true;
        }

        @Override
        public void onLoadResource(WebView  view, String  url){
            if (progressDialog == null&&!loading) {
                // in standard case YourActivity.this
                progressDialog = new ProgressDialog(WebViewActivity.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try{
                if (progressDialog!=null)
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }catch(Exception exception){
                exception.printStackTrace();
            }
        }
}

}
