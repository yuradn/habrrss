package yurasik.com.observerexample.communicator;

import yurasik.com.observerexample.model.ParseAnswer;

public interface BackendCommunicator {
	ParseAnswer postParse(String url) throws InterruptedException;
}
