package yurasik.com.observerexample.communicator;

public class CommunicatorFactory {
	public static BackendCommunicator createBackendCommunicator() {
		return new BackendCommunicatorStub();
	}
}
