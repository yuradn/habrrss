package yurasik.com.observerexample.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import yurasik.com.observerexample.model.ItemModel;

public class DB {

    private final Context mCtx;


    private DBHelper mDBHelper;
    private SQLiteDatabase mDB;

    public DB(Context ctx) {
        mCtx = ctx;
    }

    // открыть подключение
    public void open() {
        mDBHelper = new DBHelper(mCtx, ItemContract.Item.DB_NAME, null, ItemContract.Item.DB_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }

    // закрыть подключение
    public void close() {
        if (mDBHelper!=null) mDBHelper.close();
    }

    // получить все данные из таблицы DB_TABLE
    public Cursor getAllData() {
        return mDB.query(ItemContract.Item.DB_TABLE, null, null, null, null, null, null);
    }

    // добавить запись в DB_TABLE
    public void addRec(ItemModel item) {
        ContentValues cv = new ContentValues();
        cv.put(ItemContract.Item.COLUMN_TITLE,item.getTitle());
        cv.put(ItemContract.Item.COLUMN_DATE,item.getPubDate().toString());
        cv.put(ItemContract.Item.COLUMN_LINK,item.getLink());
        cv.put(ItemContract.Item.COLUMN_IMAGE,item.getImageLink());
        mDB.insert(ItemContract.Item.DB_TABLE, null, cv);
    }

    // удалить запись из DB_TABLE
    public void delRec(long id) {
        mDB.delete(ItemContract.Item.DB_TABLE, ItemContract.Item.COLUMN_ID + " = " + id, null);
    }

    // класс по созданию и управлению БД
    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                        int version) {
            super(context, name, factory, version);
        }

        // создаем и заполняем БД
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(ItemContract.Item.DB_CREATE);

//            ContentValues cv = new ContentValues();
//            for (int i = 1; i < 5; i++) {
//                cv.put(COLUMN_TXT, "sometext " + i);
//                cv.put(COLUMN_IMG, R.drawable.ic_launcher);
//                db.insert(DB_TABLE, null, cv);
//            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}