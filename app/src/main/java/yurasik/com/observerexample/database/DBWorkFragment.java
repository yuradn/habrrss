package yurasik.com.observerexample.database;


import android.content.AsyncTaskLoader;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import yurasik.com.observerexample.model.ItemModel;

public class DBWorkFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private final static String TAG = "DBWorkFragment";

    DB db;
    SimpleCursorAdapter scAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        // открываем подключение к БД
        db = new DB(getActivity());
        db.open();

        // создаем лоадер для чтения данных
        getActivity().getSupportLoaderManager().initLoader(0, null, this);
        getActivity().getSupportLoaderManager().getLoader(0).forceLoad();

    }

    public ArrayList<ItemModel> getAllItemFromDb(){
        ArrayList<ItemModel> items = new ArrayList<>();



        return items;
    }

    public void addNewItem(ItemModel item){
        // добавляем запись
        db.addRec(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), db);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG, "Finished");
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public class MyLoader extends AsyncTaskLoader<List<ItemModel>> {
        private static final String TAG = "DataLoader";

        private List<ItemModel> items;


        public MyLoader(Context context) {
            super(context);
        }



        @Override
        public List<ItemModel> loadInBackground() {
            //MyCursorLoader myCursorLoader = new MyCursorLoader();



            return null;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();

            if (items != null) {
                deliverResult(items);
            } else {
                forceLoad();
            }
        }

        @Override
        protected void onReset() {
            super.onReset();
            items = null;
        }
    }

//    static class MyCursorLoader extends CursorLoader {
//
//        DB db;
//
//        public MyCursorLoader(Context context, DB db) {
//            super(context);
//            this.db = db;
//        }
//
//        @Override
//        public Cursor loadInBackground() {
//            Cursor cursor = db.getAllData();
//            try {
//                TimeUnit.SECONDS.sleep(3);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            return cursor;
//        }
//    }

    static class MyCursorLoader extends CursorLoader {

        DB db;

        public MyCursorLoader(Context context, DB db) {
            super(context);
            this.db = db;
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = db.getAllData();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return cursor;
        }

    }

}
