package yurasik.com.observerexample.database;

import android.net.Uri;

public class ItemContract {

    private ItemContract()  {}

    public static final String AUTHORITY = "yurasik.com.observerexample.database.provider";
    public static final Uri AUTHORITY_URI = Uri.parse("content://" + AUTHORITY);


    public static final class Item {

        public Item() {}

        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/item";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/item";

        public static final String DB_NAME = "mydb";
        public static final String DB_TABLE = "mytab";
        public static final int DB_VERSION = 1;


        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, "items");

        public static final Uri TITLE_URI = Uri.withAppendedPath(AUTHORITY_URI, "title");
        public static final Uri DATE_URI = Uri.withAppendedPath(AUTHORITY_URI, "date");
        public static final Uri LINK_URI = Uri.withAppendedPath(AUTHORITY_URI, "link");
        public static final Uri IMAGE_URI = Uri.withAppendedPath(AUTHORITY_URI, "image");

        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_LINK = "link";
        public static final String COLUMN_IMAGE = "image";

        public static final String DB_CREATE =
                "create table " + DB_TABLE + "(" +
                        COLUMN_ID + " integer primary key autoincrement, " +
                        COLUMN_TITLE + " text, " +
                        COLUMN_DATE + " date, " +
                        COLUMN_LINK + " text, " +
                        COLUMN_IMAGE + " text" +
                        ");";

}
}