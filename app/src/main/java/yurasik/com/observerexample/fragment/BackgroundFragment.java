package yurasik.com.observerexample.fragment;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import yurasik.com.observerexample.database.DB;
import yurasik.com.observerexample.database.ItemContract;
import yurasik.com.observerexample.model.ItemModel;

public class BackgroundFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    private final static String TAG = BackgroundFragment.class.getSimpleName();
    private String secret="secret";
    private ArrayList<ItemModel> items;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        items = new ArrayList<>();

        DB db = new DB(getActivity());
        db.open();
        Cursor  cursor = db.getAllData();
        if (cursor .moveToFirst()) {

            while (!cursor.isAfterLast()) {
                ItemModel item = new ItemModel();

                //Title
                String title = cursor.getString(cursor
                        .getColumnIndex(ItemContract.Item.COLUMN_TITLE));
                item.setTitle(title);

                //Date
                String datePub = cursor.getString(cursor.getColumnIndex(ItemContract.Item.COLUMN_DATE));
                datePub=datePub.replace("EEST ","");
                SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd kk:mm:ss yyyy", Locale.ENGLISH);
                Date convertedDate = null;
                try {
                    convertedDate = dateFormat.parse(datePub);
                } catch (ParseException e) {
                    e.printStackTrace();
                    convertedDate=new Date();
                }

                //Thu May 21 09:01:59 EEST 2015
                item.setPubDate(convertedDate);

                //LINK
                String link = cursor.getString(cursor
                        .getColumnIndex(ItemContract.Item.COLUMN_LINK));
                item.setLink(link);

                //IMAGE
                String image = cursor.getString(cursor.getColumnIndex(ItemContract.Item.COLUMN_IMAGE));
                item.setImageLink(image);

                items.add(item);

                Log.d(TAG, item.toString());
                cursor.moveToNext();
            }
        }


    }

    public void addToDB(ArrayList<ItemModel> newItems){
        Log.d(TAG,"Add to DB");

        if (newItems==null) return;
        if (newItems.size()==0) return;
        if (items.size()==0) {
          DB db = new DB(getActivity());
          db.open();
          for (ItemModel item: newItems) {
              //Log.d(TAG, item.toString());
              db.addRec(item);
              items.add(item);
         }
            db.close();
        return;
        }


        DB db = new DB(getActivity());
        db.open();

        GregorianCalendar gOld = new GregorianCalendar();
        //Log.d(TAG,"**********");
        //Log.d(TAG,"gOld(0)= "+items.get(0).getPubDate());
        //Log.d(TAG,"gOld(size)= "+items.get(items.size()-1).getPubDate());

        gOld.setTime(items.get(items.size()-1).getPubDate());
        for (ItemModel item : newItems) {
            GregorianCalendar gNew = new GregorianCalendar();
            gNew.setTime(item.getPubDate());
            Log.d(TAG, item.getTitle() + " " + item.getPubDate());
            Log.d(TAG,"gNew.compareTo(gOld)=="+gNew.compareTo(gOld));
            Log.d(TAG,"*******");
            if (gNew.compareTo(gOld)==1) {
                //Log.d(TAG, "gNew.compareTo(gOld)==0");
                //items.add(0,item);
                items.add(item);
                Log.d(TAG,"Add item: "+item);
                db.addRec(item);
            } //else Log.d(TAG, "gNew.compareTo(gOld)="+gNew.compareTo(gOld));
        }

        db.close();

    }

    public ArrayList<ItemModel> getItems() {
        return items;
    }

    public void setItems(ArrayList<ItemModel> items) {
        this.items = items;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }


}
