package yurasik.com.observerexample.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import yurasik.com.observerexample.MainActivity;
import yurasik.com.observerexample.R;
import yurasik.com.observerexample.WebViewActivity;
import yurasik.com.observerexample.database.DB;
import yurasik.com.observerexample.model.ItemModel;

public class ListFragment extends Fragment {


    private final static String TAG = "ListFragment";
    private SimpleAdapter myAdapter;
    private final static String LIST_TITLE = "list.title";
    private final static String LIST_DATE = "list.date";


    ArrayList data;
    ListView myListView;
    ArrayList<ItemModel> items;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_layout, container, false);

        myListView = (ListView) v.findViewById(R.id.listView);

        restartView();

        return v;
    }

    private void restartView(){

        BackgroundFragment backgroundFragment = (BackgroundFragment) getActivity().getSupportFragmentManager().findFragmentByTag(MainActivity.FRAGMENT_BACKGROUND);
        if (backgroundFragment ==null) {
            backgroundFragment = new BackgroundFragment();
            FragmentTransaction manager = getActivity().getSupportFragmentManager().beginTransaction();
            manager.add(R.id.fragmentParse, backgroundFragment, MainActivity.FRAGMENT_BACKGROUND);
            manager.commit();
        }

        //items = new ArrayList<>(backgroundFragment.getItems());
        items = backgroundFragment.getItems();
        //Collections.reverse(items);

        Calendar cal = Calendar.getInstance();
        int dayofmonth  = cal.get(Calendar.DAY_OF_MONTH);
        int month       = cal.get(Calendar.MONTH);
        int year        = cal.get(Calendar.YEAR);

        data = new ArrayList<Map<String, Object>>();
        HashMap m;
        for (int i = items.size()-1; i >=0; i--) {
            m = new HashMap<String, Object>();
            Date date = items.get(i).getPubDate();

            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTime(date);

            int _day    = gregorianCalendar.get(Calendar.DAY_OF_MONTH);
            int _month  = gregorianCalendar.get(Calendar.MONTH);
            int _year   = gregorianCalendar.get(Calendar.YEAR);
            String mnth=""+_month;
            if (_month<10) mnth="0"+mnth;
            String dateString = ""+_day+"/"+mnth+"/"+_year;

            if (gregorianCalendar.get(Calendar.YEAR)==year)
                if (gregorianCalendar.get(Calendar.MONTH)==month)
                {
                    int different = dayofmonth-gregorianCalendar.get(Calendar.DAY_OF_MONTH);
                    switch (different) {
                        case 0:
                            dateString=getResources().getString(R.string.today);
                            break;
                        case 1:
                            dateString=getString(R.string.yesterday);
                            break;
                    }
                }


            int hour_int = gregorianCalendar.get(Calendar.HOUR_OF_DAY);
            String hour_str = Integer.toString(hour_int);
            if (hour_int<10) hour_str="0"+hour_str;

            int minute_int = gregorianCalendar.get(Calendar.MINUTE);
            String minute_str = Integer.toString(minute_int);
            if (minute_int<10) minute_str = "0"+minute_str;

            String time = hour_str+":"+minute_str;
            dateString+="\n"+time;

            m.put(LIST_DATE, dateString);
            m.put(LIST_TITLE, items.get(i).getTitle());
            data.add(m);
        }

        String[] from = { LIST_DATE, LIST_TITLE };
        int[] to = { R.id.textViewDate, R.id.textViewItemTitle };

        myAdapter = new SimpleAdapter(getActivity(), data, R.layout.item, from, to);
        myListView.setAdapter(myAdapter);

        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemClick " + position + " " + id);

                int pos = items.size() - 1 - position;
                Log.d(TAG, "Pos= " + pos);
//                Log.d(TAG, "Item= " + items.get(position).getLink());
//
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra(MainActivity.WEB_ADDRESS, items.get(pos).getLink());
                startActivity(intent);
            }
        });

        myListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {


                Log.d(TAG, "onItemLongClick " + position + " " + id);
                Log.d(TAG, "Delete item: "+items.get(position).getLink());

                int pos = items.size()-1-position;

                DB db = new DB(getActivity());
                db.open();
                db.delRec(id);
                db.close();

                items.remove(pos);

//                BackgroundFragment backgroundFragment = (BackgroundFragment) getActivity().getSupportFragmentManager().findFragmentByTag(MainActivity.FRAGMENT_BACKGROUND);
//                if (backgroundFragment ==null) {
//                    backgroundFragment = new BackgroundFragment();
//                    FragmentTransaction manager = getActivity().getSupportFragmentManager().beginTransaction();
//                    manager.add(R.id.fragmentParse, backgroundFragment, MainActivity.FRAGMENT_BACKGROUND);
//                    manager.commit();
//                }

//                ArrayList<ItemModel> myItems = items;
//
//                Collections.reverse(myItems);

                //backgroundFragment.setItems(items);

                changeView();

                return false;
            }
        });
    }

    public void changeView(){
        restartView();
    }


}
