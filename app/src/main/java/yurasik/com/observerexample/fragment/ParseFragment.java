package yurasik.com.observerexample.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alertdialogpro.AlertDialogPro;
import com.gc.materialdesign.views.Button;

import yurasik.com.observerexample.MainActivity;
import yurasik.com.observerexample.R;
import yurasik.com.observerexample.parse.ParseBackroundFragment;
import yurasik.com.observerexample.parse.ParseModel;


public class ParseFragment extends Fragment implements ParseModel.Observer{
    private final static String TAG = ParseFragment.class.getSimpleName();
    private static final String TAG_WORKER = "TAG_WORKER";

    AlertDialog alert;

    private ParseModel mParseModel;
    private View mProgress;
    private Button startParse;

    public ParseFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        ParseBackroundFragment parseBackroundFragment;

        mProgress = v.findViewById(R.id.view_progress);

        startParse = (Button) v.findViewById(R.id.buttonStartParse);
        startParse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mParseModel.signIn(getResources().getString(R.string.http_address));
            }
        });

        parseBackroundFragment =
                (ParseBackroundFragment) getFragmentManager().findFragmentByTag(TAG_WORKER);

        if (parseBackroundFragment != null) {
            mParseModel = parseBackroundFragment.getParseModel();
        } else {
            parseBackroundFragment = new ParseBackroundFragment();

            getFragmentManager().beginTransaction()
                    .add(parseBackroundFragment, TAG_WORKER)
                    .commit();

            mParseModel = parseBackroundFragment.getParseModel();
        }

        mParseModel.registerObserver(this);

        if (mParseModel.getParseAnswer()!=null)
            if (!mParseModel.getParseAnswer().isResult()) alertDialog(mParseModel);

        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        mParseModel.unregisterObserver(this);

        if (getActivity().isFinishing()) {
            mParseModel.stopSignIn();
        }

        if (alert!=null)
            if (alert.isShowing())
                alert.dismiss();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG,"onDestroyView");
    }

    @Override
    public void onParseStarted(ParseModel parseModel) {
        Log.i(TAG, "onParseStarted");
        showProgress(true);
    }

    @Override
    public void onParseSucceeded(ParseModel parseModel) {
        Log.i(TAG, "onParseSucceeded");
        showProgress(false);
        BackgroundFragment backgroundFragment = (BackgroundFragment) getActivity().getSupportFragmentManager().findFragmentByTag(MainActivity.FRAGMENT_BACKGROUND);
        if (backgroundFragment ==null) {
            backgroundFragment = new BackgroundFragment();
            FragmentTransaction manager = getActivity().getSupportFragmentManager().beginTransaction();
            manager.add(R.id.fragmentParse, backgroundFragment, MainActivity.FRAGMENT_BACKGROUND);
            manager.commit();
        }
        //backgroundFragment.setItems(parseModel.getParseAnswer().getItems());

        //Add new Items to DB
        backgroundFragment.addToDB(parseModel.getParseAnswer().getItems());

        ListFragment listFragment = (ListFragment) getActivity().getSupportFragmentManager().findFragmentByTag(MainActivity.FRAGMENT_LIST);
        if (listFragment==null) {
            Log.d(TAG,"list fragment=null!? **********");
            listFragment = new ListFragment();
            FragmentTransaction manager = getActivity().getSupportFragmentManager().beginTransaction();
            manager.add(R.id.fragmentDocument, backgroundFragment, MainActivity.FRAGMENT_LIST);
            manager.commit();
        }

        listFragment.changeView();


//        DBWorkFragment dbWorkFragment = (DBWorkFragment) getActivity().getSupportFragmentManager().findFragmentByTag(MainActivity.FRAGMENT_DB);
//        if (dbWorkFragment==null) {
//            dbWorkFragment = new DBWorkFragment();
//            FragmentTransaction manager = getActivity().getSupportFragmentManager().beginTransaction();
//            manager.add(dbWorkFragment, MainActivity.FRAGMENT_DB);
//            manager.commit();
//        }

//        ArrayList<ItemModel> items = parseModel.getParseAnswer().getItems();
//        for (ItemModel item : items) {
//            dbWorkFragment.addNewItem(item);
//        }
    }

    @Override
    public void onParseFailed(ParseModel parseModel) {
        Log.i(TAG, "onParseFailed");
        showProgress(false);
        alertDialog(parseModel);

    }

    private void showProgress(final boolean show) {
        startParse.setEnabled(!show);
        mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void alertDialog(final ParseModel parseModel){
        AlertDialogPro.Builder builder = new AlertDialogPro.Builder(getActivity());
        builder.setTitle("Error!")
                .setMessage(parseModel.getParseAnswer().getError())
                .setIcon(R.drawable.abc_list_selector_disabled_holo_dark)
                .setCancelable(false)
                .setNegativeButton("ОК",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                parseModel.getParseAnswer().setResult(true);
                                dialog.cancel();
                            }
                        });

        alert = builder.create();
        alert.show();
    }
}
