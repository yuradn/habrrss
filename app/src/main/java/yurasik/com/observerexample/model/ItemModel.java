package yurasik.com.observerexample.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ItemModel {
    private String title;
    private String link;
    private String imageLink;
    private Date pubDate;
    private String author;
    private List<String> categories;

    public ItemModel(){
        categories = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "ItemModel{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", imageLink='" + imageLink + '\'' +
                ", pubDate=" + pubDate +
                ", author='" + author + '\'' +
                ", categories=" + categories +
                '}';
    }
}
