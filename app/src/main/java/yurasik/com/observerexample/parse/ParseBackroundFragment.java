package yurasik.com.observerexample.parse;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class ParseBackroundFragment extends Fragment {
    public final ParseModel parseModel;


    public ParseBackroundFragment() {
        this.parseModel = new ParseModel();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public ParseModel getParseModel(){
        return parseModel;
    }
}
